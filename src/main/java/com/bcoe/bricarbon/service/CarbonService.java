package com.bcoe.bricarbon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bcoe.bricarbon.common.R;
import com.bcoe.bricarbon.entity.CarbonEntity;
import com.bcoe.bricarbon.vo.ReqNewCarbon;
import com.bcoe.bricarbon.vo.ReqSign;

/**
 *
 *
 * @author he_jiebing@jiuyv.com
 * @date 2021-04-20 15:58:29
 */
public interface CarbonService extends IService<CarbonEntity> {

    /**
     * 建链
     * @param reqNewCarbon
     * @return
     */
    R newCarbon(ReqNewCarbon reqNewCarbon);

    /**
     * 签名
     * @param reqSign
     * @return
     */
    R sign(ReqSign reqSign);
}

