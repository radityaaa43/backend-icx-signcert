package com.bcoe.bricarbon.vo;

import java.io.Serializable;

public class ReqCarbonItem implements Serializable {

    private static final long serialVersionUID = -5913486136685829601L;
    private String projectName;

    private String projectID;

    private Integer creditAmount;

    private String carbonYearFrom;

    private String carbonYearTo;

    private String saleType;

    private Integer offerPrice;

    private String coBenefit;

    private String sdgNumber;

    private String corsiaCompliant;

    private String verraRegistryLink;

    private String verraRegistryAccount;

    private String verraRegistryNo;

    private String imageProject;

    private String participaterId;

    //GET & SET Variabel

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectID() { return projectID; }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCarbonYearFrom() { return carbonYearFrom; }

    public void setCarbonYearFrom(String carbonYearFrom) {
        this.carbonYearFrom = carbonYearFrom;
    }

    public String getCarbonYearTo() { return carbonYearTo; }

    public void setCarbonYearTo(String carbonYearTo) {
        this.carbonYearTo = carbonYearTo;
    }

    public String getSaleType() { return saleType; }

    public void setSaleType(String saleType) { this.saleType = saleType; }

    public Integer getOfferPrice() { return offerPrice; }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCoBenefit() { return coBenefit; }

    public void setCoBenefit(String coBenefit) {
        this.coBenefit = coBenefit;
    }

    public String getSdgNumber() { return sdgNumber; }

    public void setSdgNumber(String sdgNumber) {
        this.sdgNumber = sdgNumber;
    }

    public String getCorsiaCompliant() { return corsiaCompliant; }

    public void setCorsiaCompliant(String corsiaCompliant) {
        this.corsiaCompliant = corsiaCompliant;
    }

    public String getVerraRegistryLink() { return verraRegistryLink; }

    public void setVerraRegistryLink(String verraRegistryLink) {
        this.verraRegistryLink = verraRegistryLink;
    }

    public String getVerraRegistryAccount() { return verraRegistryAccount; }

    public void setVerraRegistryAccount(String verraRegistryAccount) {
        this.verraRegistryAccount = verraRegistryAccount;
    }

    public String getVerraRegistryNo() { return verraRegistryNo; }

    public void setVerraRegistryNo(String verraRegistryNo) {
        this.verraRegistryNo = verraRegistryNo;
    }

    public String getImageProject() { return imageProject; }

    public void setImageProject(String imageProject) {
        this.imageProject = imageProject;
    }

    public String getParticipaterId() { return participaterId; }

    public void setParticipaterId(String participaterId) {
        this.participaterId = participaterId;
    }

}
