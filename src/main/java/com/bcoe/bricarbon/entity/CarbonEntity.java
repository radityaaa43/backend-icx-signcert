package com.bcoe.bricarbon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 * @author he_jiebing@jiuyv.com
 * @date 2021-04-20 15:58:29
 */
@TableName("carbon")
public class CarbonEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;//
    private Date insertedAt;//
    private Date updatedAt;//
    private String signStatus;//
    private String userId;//
    private String role;//
    private String carbonDesc;//
    private String projectName;//
    private String projectId;//
    private String issuer;
    private Integer creditInitial;
    private Integer creditAmount;//
    private String carbonYearFrom;//
    private String carbonYearTo;//
    private Integer offerPrice;//
    private String coBenefit;//
    private String sdgNumber;
    private String registryLink;
    private String registryAccount;
    private String registryNo;
    private String imageProject;//
    private String participaterId;//
    private Integer isSigned;//


    //GET & SET Variabel
    public Integer getIsSigned() { return isSigned; }
    public void setIsSigned(Integer isSigned) { this.isSigned = isSigned; }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectId() { return projectId; }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
    public String getIssuer() {return issuer;}
    public void setIssuer(String issuer) { this.issuer = issuer;}
    public Integer getCreditInitial() {
        return creditInitial;
    }

    public void setCreditInitial(Integer creditInitial) {
        this.creditInitial = creditInitial;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCarbonYearFrom() { return carbonYearFrom; }

    public void setCarbonYearFrom(String carbonYearFrom) {
        this.carbonYearFrom = carbonYearFrom;
    }

    public String getCarbonYearTo() { return carbonYearTo; }

    public void setCarbonYearTo(String carbonYearTo) {
        this.carbonYearTo = carbonYearTo;
    }


    public Integer getOfferPrice() { return offerPrice; }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCoBenefit() { return coBenefit; }

    public void setCoBenefit(String coBenefit) {
        this.coBenefit = coBenefit;
    }

    public String getSdgNumber() { return sdgNumber; }

    public void setSdgNumber(String sdgNumber) {
        this.sdgNumber = sdgNumber;
    }


    public String getRegistryLink() { return registryLink; }

    public void setRegistryLink(String registryLink) {
        this.registryLink = registryLink;
    }

    public String getRegistryAccount() { return registryAccount; }

    public void setRegistryAccount(String registryAccount) {
        this.registryAccount = registryAccount;
    }

    public String getRegistryNo() { return registryNo; }

    public void setRegistryNo(String registryNo) {
        this.registryNo = registryNo;
    }

    public String getImageProject() { return imageProject; }

    public void setImageProject(String imageProject) {
        this.imageProject = imageProject;
    }

    public String getParticipaterId() { return participaterId; }

    public void setParticipaterId(String participaterId) {
        this.participaterId = participaterId; }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getCarbonDesc() {
        return carbonDesc;
    }

    public void setCarbonDesc(String carbonDesc) {
        this.carbonDesc = carbonDesc;
    }

    public Date getInsertedAt() {
        return insertedAt;
    }
    public void setInsertedAt(Date insertedAt) {
        this.insertedAt = insertedAt;
    }
    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public String getSignStatus() {
        return signStatus;
    }
    public void setSignStatus(String signStatus) {
        this.signStatus = signStatus;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
