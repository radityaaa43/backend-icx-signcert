package com.bcoe.bricarbon.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.bcoe.bricarbon.service.CarbonService;
import com.bcoe.bricarbon.common.R;
import com.bcoe.bricarbon.vo.ReqNewCarbon;
import com.bcoe.bricarbon.vo.ReqSign;

@RestController
@RequestMapping("brichain/carbon")
public class CarbonController {
    @Autowired
    private CarbonService carbonService;

    @PostMapping("/new")
    public R newCarbon(@RequestBody ReqNewCarbon reqNewCarbon) { return carbonService.newCarbon(reqNewCarbon); }

   @PostMapping("/sign")
   public R sign(@RequestBody ReqSign reqSign) { return carbonService.sign(reqSign); }

    @PostMapping
}
